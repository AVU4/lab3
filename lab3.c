#include <stdio.h>

const int array_first[] = {1, 2, 3};
const int array_second[] = {4, 5, 6};

int scalar(const int* array_first, const int* array_second, size_t count) {
	size_t i;
	int sum = 0;
	for (i = 0; i < count; i ++)
		sum += array_first[i] * array_second[i];
	return sum;
}

int is_prime(unsigned long n){
	unsigned long divider;

	for (divider = 2; divider <= n/2; divider ++){
		if (n % divider == 0){
			return 0;
		}
	}
	if (n == 1) return 0;
	return 1;
}

int main(int argc) {
	printf("The scalar product is : %d \n", scalar(array_first, array_second, sizeof(array_first)/sizeof(int)));
	unsigned long input;
	scanf("%lu", &input);
	if (is_prime(input) == 1)
		printf("%s\n", "yes");
	else
		printf("%s\n", "no");

	return 0;
}
